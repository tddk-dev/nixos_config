{pkgs, pkgs-stable, ...}: {
  environment.systemPackages = with pkgs; [
    audacity
    blender
    discord-canary    
    firefox
    gparted   
    kdePackages.kdeconnect-kde    
    mpv
    neovim
    pkgs-stable.obsidian
    rofi-wayland
    steam
    vscodium-fhs
    wofi
    krita
  ];
}
