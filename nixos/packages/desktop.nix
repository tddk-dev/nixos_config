{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    # wayland
    cliphist
    gnome.gdm
    wl-clipboard
    xwayland
    libsForQt5.full
    # window managers
    hyprland
    polybar
    waybar
    xdg-desktop-portal-kde
    # xdg-desktop-portal-hyprland
  ];
}
