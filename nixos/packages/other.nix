{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    home-manager
    libsForQt5.qt5ct
    libsForQt5.qtstyleplugin-kvantum
    papirus-nord
  ];
}