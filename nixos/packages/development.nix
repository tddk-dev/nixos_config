{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    corepack_22
    direnv
    gcc
    go
    gnumake
    nil
    nodejs
    python
    pyright
    python312Packages.mkdocs
    python312Packages.mkdocs-material
    (python3.withPackages (ps: with ps; [requests]))
  ];
}
