{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    feh
    # screenshotting
    flameshot
    grim
    grimblast
    slurp
    swappy
  ];
}