{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    alacritty
    kitty
    nerdfonts
    tmux
    starship
    oh-my-posh
    zsh
    zsh-syntax-highlighting
    zsh-completions
    zsh-autosuggestions
  ];
}
