{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    pipewire
    pulseaudioFull
    pavucontrol  
    pamixer
    keyd
    actkbd
    xboxdrv
  ];
}
