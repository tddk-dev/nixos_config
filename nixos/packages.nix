{...}: {
  nixpkgs.config = {
    allowUnfree = true;
    permittedInsecurePackages = ["python-2.7.18.8" "electron-25.9.0"];
  };

  imports = [
    ./packages/applications.nix
    ./packages/cli_utils.nix
    ./packages/desktop.nix
    ./packages/development.nix
    ./packages/gui_utils.nix
    ./packages/os_utils.nix
    ./packages/other.nix
    ./packages/shell.nix
    ./packages/xfce.nix
  ];
}
