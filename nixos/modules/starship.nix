{
  programs.starship = {
    enable = true;
    # enableZshIntegration = true;
    # enableBashIntegration = false;

    presets = [
      "no-empty-icons"
      "nerd-font-symbols"
    ];

    settings = {
      "$schema" = "https://starship.rs/config-schema.json";

      sudo = {
        disabled = false;
        symbol = "🧙 ";
        style = "bold blue";
      };

      package.disabled = true;

      shell = {
        disabled = false;
        format = "$indicator";
        fish_indicator = "";
        bash_indicator = "[bash](bright-white) ";
        zsh_indicator = "[zsh](bright-white) ";
      };
      
    };
  };
}