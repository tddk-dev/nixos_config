{ ... }: {

  users.users.derek = {
    isNormalUser = true;
    description = "derek";
    extraGroups = [ "networkmanager" "wheel" "input" "libvirtd" ];
  };
}
