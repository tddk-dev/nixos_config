{
  imports = [
    ./modules/actkbd.nix
    ./modules/bluetooth.nix
    ./modules/bootloader.nix
    ./modules/env.nix
    ./modules/nm.nix
    ./modules/networking.nix
    ./modules/sound.nix
    ./modules/tmux.nix
    ./modules/user.nix
    ./modules/windowmanager/xfce.nix
    ./modules/windowmanager/wayland.nix
    ./modules/windowmanager/xorg.nix
  ];

  disabledModules = [
    ./modules/nvidia.nix
    ./modules/starship.nix
    ./modules/steam.nix
    ./modules/windowmanager/kde.nix
    ./modules/windowmanager/greetd.nix
    ./modules/windowmanager/hyprland.nix
  ];
}
