{
  imports = [
    ./modules/alacritty.nix
    ./modules/git.nix
    ./modules/kitty.nix
    ./modules/windowmanager.nix
    ./modules/shell.nix
    #vscode
    ./modules/vscode/vscode.nix
  ];

  disabledModules = [
    #hyprland
    ./hyprland/hyprland.nix
    ./hyprland/waybar.nix
  ];
}
