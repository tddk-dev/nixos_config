{
  programs.git = {
    enable = true;
    userName = "derek";
    userEmail = "github.release078@simplelogin.com";
    extraConfig = {
      init.defaultBranch = "main";
      credential.helper = ["cache --timeout 21600" "oauth"];
    };
    
  };
}

  