{ ... }:
  let
    enable = true;
    initExtra = 
      ''
      export PATH=$PATH:$HOME/go/bin
      
      gitac() {
          if [ -z "$1" ]; then
              echo "Commit message is required"
              return 1
          fi
          git add .
          git commit -m "$*"
        }

      '';
    shellAliases = {
      ".." = "cd ..";
      ll = "ls -l --color";
      lla = "ls -la --color";
      edshell = "$EDITOR /home/derek/nix/nixos/modules/shells.nix";
      scbash = "source ~/.bashrc";
      gits = "git status";
      nixf = 
      ''
        echo "running cmd nix flake update ~/nix/";
        nix flake update ~/nix/;
      '';

      #home-manager rebuild shortcut
      hmsw = 
      ''
        echo "running cmd home-manager switch --flake ~/nix/";
        home-manager switch --flake ~/nix/;
      '';

      #nixos rebuild shortcut
      nixsw = 
      ''
        echo "running cmd - sudo nixos-rebuild switch --flake ~/nix/";
        sudo nixos-rebuild switch --flake ~/nix/;
      '';
      
      #open codium in the nix package workspace
      ednix = "codium ~/nix/";
    };

  in {
  programs = {
    
    bash = {
      inherit enable;
      inherit initExtra;
      inherit shellAliases;
    };

    zsh = {
      inherit enable;
      inherit initExtra;
      inherit shellAliases;
    };
  };
}