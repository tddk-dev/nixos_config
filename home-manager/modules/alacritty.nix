{
  programs.alacritty = {
    enable = true;
    settings = {
      cursor = {
        style = { shape = "Beam"; };
        thickness = 0.20;
      };
      window.opacity = 0.85;
      window.padding = {
        x = 5;
        y = 5;
      };
      font = {
        bold = {
          family = "JetBrainsMono Nerd Font";
          style = "Bold";
        };
        bold_italic = {
          family = "JetBrainsMono Nerd Font";
          style = "Bold Italic";
        };
        italic = {
          family = "JetBrainsMono Nerd Font";
          style = "Italic";
        };
        normal = {
          family = "JetBrainsMonoNL Nerd Font";
          style = "Regular";
        };
      };

      colors.primary.background = "#1d2021";
    };
  };
}