{
  programs.kitty = {
    enable = true;
    shellIntegration.enableBashIntegration = true;
    shellIntegration.enableZshIntegration = true;
    settings = {
    };
    font = {
      name = "JetBrainsMono";
    };
    keybindings = {
    };
  };
}