{pkgs, ...}: {
  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    userSettings = {
      "files.autoSave" = "off";
      "[nix]"."editor.tabSize" = 2;
      "nix.enableLanguageServer" = true;
      "nix.serverPath" = "nil";
      "workbench.startupEditor"= "none";
      "workbench.colorTheme"= "Dracula";
      "editor.fontFamily"= "JetBrainsMono Nerd Font";
      "editor.minimap.enabled"= false;
    };

    userTasks = {
      type = "shell";
      label = "Hello task";
      command = "hello";
    };

    extensions = with pkgs.vscode-extensions; [
      #generic
      alefragnani.bookmarks
      #yatki.vscode-surround
      #themes
      dracula-theme.theme-dracula
      #nix extensions
      mkhl.direnv
      bbenoist.nix
      jnoortheen.nix-ide
      #go
      golang.go
      #python
      ms-python.python
      ms-pyright.pyright
      njpwerner.autodocstring
      charliermarsh.ruff
    ];
  };
}
